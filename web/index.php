<?php
require __DIR__ . '/../vendor/autoload.php';

$trees = (new \Packages\HR\TreeFactory())->buildTree(new \Packages\HR\SomeDataProvider());


$iterator = new RecursiveIteratorIterator($trees, RecursiveIteratorIterator::SELF_FIRST);


$View = new \Packages\HR\View\Classic($iterator);

$table_data = [];
$text = '';

$iterator->rewind();

while ($iterator->valid())
{
    $text.= $View->getText();
    $table_data[]=[
        'id' => $View->getId(),
        'name' => $View->getName(),
        'email' => $View->getEmail(),
        'value' => $View->getValue(),
        'child_value' => $View->getWithChildValue(),
        'text' => $View->getText(),
        'original_name' => $View->getOriginalName()
    ];
    $iterator->next();
}

?>
<html>
<head>
    <style>
        .invalid {
            color: red;
        }
        table {
            width: 100%; /* Ширина таблицы */
            border: 4px black; /* Рамка вокруг таблицы */
            border-collapse: collapse; /* Отображать только одинарные линии */
        }
        th {
            text-align: left; /* Выравнивание по левому краю */
            background: #ccc; /* Цвет фона ячеек */
            padding: 5px; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
        }
        td {
            padding: 0; /* Поля вокруг содержимого ячеек */
            border: 1px solid black; /* Граница вокруг ячеек */
        }
        tr:hover {
            background: yellowgreen;
        }

        div.text_block {
            display: none;
            position: fixed;
            left: 50%;
            top:0;
            padding: 10px;
        }
        /*tr:hover > td.text > div {*/
            /*display: block;*/
        /*}*/

        h1 {
            margin: 0;
        }

        input[type=radio] {
            position: absolute;
            top: -9999px;
            left: -9999px;
        }

        label {
           display: block;
            padding: 5px;
        }

         /*Toggled State */
        input[type=radio]:checked + div {
            display: block;
        }
    </style>
</head>
<body>
<div style="float:left; width:50%;">
    <table>
        <tr>
            <th>ID</th>
            <th>Имя</th>
            <th>email</th>
            <th>Часы</th>
            <th>Сумма с подчиненными</th>
        </tr>
        <?php foreach ($table_data as $tr) { ?>

            <tr>
                <td>
                    <label for="toggle-<?=$tr['id'];?>">
                    <?=$tr['id'];?>
                    </label>
                </td>
                <td>
                    <label for="toggle-<?=$tr['id'];?>">
                    <?=$tr['name'];?>
                    </label>
                </td>
                <td>
                    <label for="toggle-<?=$tr['id'];?>">
                    <?=$tr['email'];?>
                    </label>
                </td>
                <td>
                    <label for="toggle-<?=$tr['id'];?>">
                    <?=$tr['value'];?>
                    </label>
                </td>

                <td>
                    <label for="toggle-<?=$tr['id'];?>">
                    <?=$tr['child_value'];?>
                    </label>
                </td>

            </tr>

        <?php } ?>
    </table>
</div>
<form action="">
<?php foreach ($table_data as $tr) { ?>
        <input type="radio" name="display" id="toggle-<?=$tr['id'];?>">
        <div class="text_block">
            <h1><?=$tr['original_name'];?></h1>
            <?=$tr['text'];?>
        </div>


<?php } ?>
</form>


</body>
</html>


