<?php
namespace Packages\HR;

use App\Database\DB;

/**
 * Created by PhpStorm.
 * User: Artem
 * Date: 13.04.2017
 * Time: 23:08
 */
class SomeDataProvider
{

    public static function getData()
    {
        $result = DB::getInstance()->getConnection()->query('SELECT * FROM tbldata ORDER BY parent, id');
        $data = [];
        while ($row = $result->fetch_assoc()) {
            $data[$row['id']] = $row;
        }
        return $data;
    }
}