<?php


namespace Packages\HR\View;

use Packages\HR\SomeDataObject;

class Classic
{

    protected $Iterator;

    public function __construct(\RecursiveIteratorIterator $Iterator)
    {
        $this->Iterator = $Iterator;
    }

    /**
     * @return SomeDataObject
     */
    protected function getCurrent()
    {
        return $this->Iterator->current();
    }

    public function getId()
    {
        return $this->getCurrent()->getId();
    }

    public function getOriginalName()
    {
        return $this->getCurrent()->getName();
    }

    public function getName()
    {

        $pad = "&emsp;";
        return str_pad('', $this->Iterator->getDepth() * mb_strlen($pad), $pad) . $this->getCurrent()->getName();
    }

    public function getEmail()
    {
        $email = $this->getCurrent()->getEmail();
        $email_valid = filter_var($this->getCurrent()->getEmail(), FILTER_VALIDATE_EMAIL);
        $class = 'valid';
        if ($email_valid === false) $class = 'invalid';
        $email = $email_valid ? $email_valid : '(incorrect!) ' . $email;
        return "<span class=\"$class\">$email</span>";
    }

    public function getValue()
    {
        return $this->getCurrent()->getValue();
    }

    public function getWithChildValue()
    {
        return $this->getCurrent()->getWithChildValue();
    }

    public function getText()
    {
        return $this->getCurrent()->getText();
    }

}