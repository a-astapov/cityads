<?php

namespace Packages\HR;


class TreeFactory
{

    protected $register = [];
    protected $data_set = [];

    /**
     * @param SomeDataProvider $someDataProvider
     * @return TreeContainer;
     */
    public function buildTree(SomeDataProvider $someDataProvider)
    {
        $tree = new TreeContainer();
        $this->data_set = $someDataProvider->getData();
        $this->register = [];
        foreach (array_keys($this->data_set) as $id) {
            $Object = $this->getSomeObject($id);
            if (!$Object->getParentId()) {
                $tree->add($Object);
                continue;
            }

            $Parent = $this->getSomeObject($Object->getParentId());
            $Parent->add($Object);
        }

        return $tree;

    }

    /**
     * @param $id
     * @return SomeDataObject
     */
    protected function getSomeObject($id)
    {
        if (isset($this->register[$id])) return $this->register[$id];
        $this->register[$id] = new SomeDataObject($this->data_set[$id]);
        return $this->register[$id];
    }
}