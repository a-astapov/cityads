<?php

namespace Packages\HR;


class SomeDataObject implements \RecursiveIterator
{

    /**
     * @var self|null;
     */
    private $parent;
    /**
     * @var self[];
     */
    private $childes = [];

    private $id;
    private $name;
    private $email;
    private $value;
    private $text;
    private $parent_id;


    public function next()
    {
        return next($this->childes);
    }

    /**
     * @return self
     */
    public function current()
    {
        return current($this->childes);
    }

    public function rewind()
    {
        return reset($this->childes);
    }

    public function key()
    {
        return key($this->childes);
    }

    public function valid()
    {
        return current($this->childes) !== false;
    }

    public function hasChildren()
    {
        return $this->current()->hasChildes();
    }

    public function getChildren()
    {
        return $this->current();
    }

    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->value = $data['value'];
        $this->text = $data['text'];
        $this->parent_id = $data['parent'];

    }


    public function getParent()
    {
        return $this->parent;
    }

    public function getChildes()
    {
        return $this->childes;
    }

    public function hasChildes()
    {
        return !empty($this->childes);
    }


    public function add(self $someDataObject)
    {
        if (!isset($this->childes[$someDataObject->getId()])) {
            $this->childes[$someDataObject->getId()] = $someDataObject;
            $someDataObject->parent = $this;
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getWithChildValue()
    {
        $sum = $this->getValue();
        foreach ($this->getChildes() as $Leaf) {
            /**
             * @var $Leaf SomeDataObject
             */
            $sum += $Leaf->getWithChildValue();
        }
        return $sum;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getParentId()
    {
        return $this->parent_id;
    }
}