<?php

namespace Packages\HR;


class TreeContainer implements \RecursiveIterator
{
    /**
     * @var SomeDataObject[]
     */
    protected $trees = [];

    /**
     * @var SomeDataObject
     */
    protected $current;

    public function add(SomeDataObject $someDataObject)
    {
        $this->trees[] = $someDataObject;
    }

    public function getAll()
    {
        return $this->trees;
    }

    public function next()
    {
        return next($this->trees);
    }

    /**
     * @return SomeDataObject
     */
    public function current()
    {
        return current($this->trees);
    }

    public function rewind()
    {
        return reset($this->trees);
    }

    public function key()
    {
        return key($this->trees);
    }

    public function valid()
    {
        return current($this->trees) !== false;
    }

    public function hasChildren()
    {
        return $this->current()->hasChildes();
    }

    public function getChildren()
    {
        return $this->current();
    }


}